# issue logger

For the issue logger plugin to work as efficient as possible, your components hsould have a gitUrl property, pointing to the git repo, and a newIssue url pointing to the tracking tool you use to manage your issues for this compoenent. (typically the github, or gitlab deeplink to the issue creation page).

![issuelogger](https://gitlab.com/fiddlebe/ui5/plugins/issuelogger/uploads/c5eed04631b2d3cc558c68066a53ed9b/issuelogger.gif)
